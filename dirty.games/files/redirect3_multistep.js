/*var urls = [
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=1&kw="+page_name+"-1",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=2&kw="+page_name+"-2",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=3&kw="+page_name+"-3",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=4&kw="+page_name+"-4",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=5&kw="+page_name+"-5",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=6&kw="+page_name+"-6",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=7&kw="+page_name+"-7",
	"https://www.securegfm.com/c8e3795a-1a23-46dc-b14c-37de8cf9be61?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=8&kw="+page_name+"-8"
];*/

var urls = [
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=1&kw="+page_name+"-1",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=2&kw="+page_name+"-2",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=3&kw="+page_name+"-3",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=4&kw="+page_name+"-4",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=5&kw="+page_name+"-5",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=6&kw="+page_name+"-6",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=7&kw="+page_name+"-7",
	"https://www.securegfm.com/25d874e0-1621-4aa6-9361-e4568cd24ab0?SID="+SID+"&SID2="+SID2+"&SID3="+SID3+"&SID4="+SID4+"&id=8&kw="+page_name+"-8"
];

var detectBackOrForward = function(onBack, onForward) {
  hashHistory = [window.location.hash];
  historyLength = window.history.length;

  return function() {
    var hash = window.location.hash, length = window.history.length;
    if (hashHistory.length && historyLength == length) {
      if (hashHistory[hashHistory.length - 2] == hash) {
        hashHistory = hashHistory.slice(0, -1);
        onBack();
      } else {
        hashHistory.push(hash);
        onForward();
      }
    } else {
      hashHistory.push(hash);
      historyLength = length;
    }
  }
};

window.addEventListener("hashchange", detectBackOrForward(
  function() { refreshSession(); },
  function() {  }
));

window.onpageshow = function (evt) {
	if (evt.persisted) {
		document.body.style.display = "none";
		location.reload();
	}
};
function refreshSession() {
	  
	$.each(urls, function( index, value ) {
		if(window.sessionStorage.getItem(page_name+'-'+value)) {
			var url = urls[index];
			window.sessionStorage.removeItem(page_name+'-'+value);
			redirectToUrl(url);
			return false;
		}
	});
}

function initialize() {
	$.each(urls, function( index, value ) {
		var value = urls[index];
		window.sessionStorage.setItem(page_name+'-'+value, 1);		
	});
}

function redirectToUrl(url){
	setTimeout(function (url) {
		window.location.href = url;		
	}, 10, url);
}
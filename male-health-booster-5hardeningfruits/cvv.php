<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
		<title>Card Validation Code</title>
		<style type="text/css">
			body { background: #FFF !important; font-family: Helvetica, Arial, Verdana, sans-serif; font-size: 12pt; }
			#container { padding: 10px; }	
		</style>
	</head>
	<body>
		<div id="wrap">
			<div id="container">
                            Most cards have a <b>3-digit</b> code printed on the <b>back</b> of the card, to the right of the card number.
                            <br><br><br>
                            <center><img src="cvv.gif" alt="CVV Location"></center>
			</div>
		</div>
	</body>
</html>

(function ($) {
  $.fn.emc = function (options) {

    var defaults = {
        key: [],
        scoring: "normal",
        progress: true
      },
      settings = $.extend(defaults, options),
      $quizItems = $('[data-quiz-item]'),
      itemCount = $quizItems.length,
      chosen = [];

    emcInit();

    if (settings.progress) {
      var $bar = $('#emc-progress'),
        $inner = $('<div id="emc-progress_inner"></div>'),
        $perc = $('<span id="emc-progress_ind">0/' + itemCount + '</span>');
      $bar.append($inner).prepend($perc);
    }

    function emcInit() {
      $quizItems.each(function (index) {
        var $this = $(this),
          $choiceEl = $this.find('.choices'),
          choices = $choiceEl.data('choices');
        percents = $choiceEl.data('percent');
        old = $choiceEl.data('old');

        for (var i = 0; i < choices.length; i++) {
          if (index == 2) {
            $option = $('<button class="answer next-btn" data-percent="' + percents[i] + '" data-id="' + index + '" data-scroll="#top" value="' + old[i] + '" onclick="Howold(this.value)">');
            $label = $('<span class="bar" data-percent="' + percents[i] + '"></span>');
            $label2 = $('<span class="answer-text">' + choices[i] + '</span>');
            $option.append($label);
            $option.append($label2);
            $choiceEl.append($option);
            $('#slide-2 button.next-btn').on('click', function() {
              $(this).addClass('answer-how-old');
              $(this).attr('data-percent', '38');
              $(this).children('.bar').attr('data-percent', '38');
              let i = 0;
              $('#slide-2 button.next-btn:not(.answer-how-old)').each(function() {
                  let percent = [18, 31, 13];
                  $(this).attr('data-percent', percent[i]);
                  $(this).children('.bar').attr('data-percent', percent[i]);
                  i++;
              });
          });      
          } else {
            $option = $('<button class="answer next-btn" data-percent="' + percents[i] + '" data-id="' + index + '" data-scroll="#top">');
            $label = $('<span class="bar" data-percent="' + percents[i] + '"></span>');
            $label2 = $('<span class="answer-text">' + choices[i] + '</span>');
            $option.append($label);
            $option.append($label2);
            $choiceEl.append($option);
          };

          $option.on('click', function () {
            $this.addClass('item-correct');
            $this.removeClass('next-quest');
            if ($('.dontUsed').length == 1 && $this.hasClass('dontUsed')) {
              $this.addClass('focus-quest');
            } else {};

            if (!($('section').hasClass('start-quizz'))) {
              $('a:not(.cross)').attr('data-scroll', '.next-quest');
              $('a').addClass('nonvitr');
              $('a').addClass('nonquizz');
              $('a').removeAttr("href");
              $('.btn').css('pointer-events', 'auto');
              $('.btn').attr("href", "//" + offerDomain + "/click.php?lp=1");
              $('footer').css('display', 'block');
              setTimeout(function () {
                $('footer').css('opacity', '1');
              }, 100);
              $('footer').fadeIn('slow', 'linear');
              $('#ssbutton').html('<span class="en">Answer all the questions and get a discount</span><span class="es">Responde a todas las preguntas y obtener un descuento en la botella</span>');
              $('#ssbutton').attr('id', 'ssbutton-fix');
            };
            $('section').addClass('start-quizz');
            $this.removeClass('dontUsed');
            return getChosen();
          });
        }
      });
    }

    function getChosen() {
      $quizItems.each(function (index) {
        if ($(this).hasClass('item-correct')) {
          if (chosen.includes(index)) {
            return
          } else {
            chosen.push(index);
          }
        };
      });
      getProgress();
    }

    function getProgress() {
      var prog = (chosen.length / itemCount) * 100 + "%",
        $submit = $('#emc-submit');
      if (settings.progress) {
        $perc.html('<span class="en">You got a </span><span class="es">Tienes un </span>' + Math.round(chosen.length * 50 / 5) + '<span class="en">% discount</span><span class="es">% de descuento</span>');
        $inner.css({
          width: prog
        });
      }
      if (chosen.length === itemCount) {

        // cookie
        var expires = new Date();
        expires.setDate(expires.getDate() + 7);
        expires.toUTCString();
        var quizzPass = 'quizzpass';
        document.cookie = [
          'quizzPass=' + quizzPass, '; expires=' + expires // Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÐµÐ¼ expires, max-age Ð½Ðµ Ð¿Ð¾Ð´Ð´ÐµÑ€Ð¶Ð¸Ð²Ð°ÐµÑ‚ÑÑ Ð² Ð˜Ð•
          // , '; path=/'
          , '; domain=' + document.location.host
        ].join('');
        // end cookie

        setTimeout(function () {
          $submit.addClass('ready-show');
          $('.question').css('display', 'none');
          $('.question-num').css('display', 'none');
          $('.choices').css('display', 'none');
        }, 2000);

        setTimeout(function () {
          $('#emc-progress').css('opacity', '0');
          $('#next-min').css('opacity', '0');
          setTimeout(function () {
            $('#emc-progress').css('display', 'none');
            $('#next-min').css('display', 'none');
            $('#progress-button').css('display', 'block');
            setTimeout(function () {
              $('#progress-button').css('opacity', '1');
            }, 100);
          }, 150);
        }, 1000);

        setTimeout(function () {
          $('.stock-container').css('display', 'block');
          $('.stock').css('display', 'block');
          // $('a').css('pointer-events', 'auto');
          $('#quest-box section:not(.focus-quest)').css('display', 'none');
          // $('img:not(.cross)').wrap('<a href=""></a>');
          $('a').removeClass('nonvitr');
          $('a').attr("href", "//" + offerDomain + "/click.php?lp=1");
          $('section').removeClass('item-correct');
          if (!($('#quest-box').is(':empty'))) {
            $('#ssbutton-fix').css('display', 'none');
          } else {
            $('#ssbutton-fix').attr('id', 'ssbutton');
            $('#ssbutton').html('<span class="en">Get your discount</span><span class="es">Obtenga su descuento</span>');
          };
          $('html, body').animate({
            scrollTop: $('.focus-quest').offset().top-50
          }, 500);
        }, 2000);
      }
    }
  }
}(jQuery));


$(document).emc({});

// Ð°Ð½Ð¸Ð¼Ð°Ñ†Ð¸Ñ ÐºÐ²Ð¸Ð·Ð°
$(document).ready(function () {
  let slideId;

  $('.next-btn').on('click', function () {
    slideId = $(this).attr('data-id');
    $('#slide-' + slideId + ' button').removeClass('selected');
		$(this).addClass('selected');
    $('#slide-' + slideId).addClass('complete');
    $('#slide-' + slideId + ' button').addClass('complete');
    $('#slide-' + slideId + ' button').css('pointer-events', 'none');
    $('#slide-' + slideId + ' button .bar').each(function () {
      $(this).css('width', $(this).attr('data-percent') + '%');
    });
    $('#slide-' + slideId + ' button').each(function () {
      let numb_finish = $(this).attr('data-percent');
      $(this).attr('data-percent', 0).animate({
        Counter: numb_finish
      }, {
        duration: 1500,
        easing: 'linear',
        step: function (now) {
          $(this).attr('data-percent', Math.ceil(now));
        }
      });
    });
  });
});
// ÐºÐ¾Ð½ÐµÑ† Ð°Ð½Ð¸Ð¼Ð°Ñ†Ð¸Ð¸ Ð¾Ñ‚Ð²ÐµÑ‚Ð¾Ð² ÐºÐ²Ð¸Ð·Ð°

//ÐŸÐµÑ€ÐµÐ½Ð¾Ñ Ðº Ð½ÐµÐ¾Ñ‚Ð²ÐµÑ‡ÐµÐ½Ð½Ñ‹Ð¼ Start
var target = $('#quest-box');
var targetPos;
var winHeight;
var scrollToElem;

setTimeout(function () {
  // target = $('#quest-box');
  targetPos = target.offset().top;
  winHeight = $(window).height();
  scrollToElem = targetPos - winHeight - 50;
  oneTimeScroll = false;
  $(window).scroll(function () {
    var winScrollTop = $(this).scrollTop();

    if (winScrollTop > scrollToElem) {
      if ($('.dontUsed').length != 0) {
        $('.next-min').css('display', 'flex');
      };
      $('#emc-progress_ind').css('width', 'calc(100% - 50px)');
      $('#emc-progress').css('width', 'calc(100% - 50px)');
      if ($('section').hasClass('start-quizz')) {
        $('#description-quest').css('display', 'none');
        $('#quest-box').append($('.dontUsed'));
      };

      if (!oneTimeScroll && $('section').hasClass('start-quizz') && $('.dontUsed').length != 0) {
        $('html, body').animate({
          scrollTop: $('#ssbutton-fix').offset().top
        }, 500);
        oneTimeScroll = true;
      };
    };
  });
}, 2000);

//Ð”Ð¾Ð¿Ð¾Ð»Ð½ÐµÐ½Ð¸Ðµ Ðº Ð¿ÐµÑ€ÐµÐºÐ»ÑŽÑ‡ÐµÐ½Ð¸ÑŽ ÑÐ·Ñ‹ÐºÐ°
$('#switcher-en').click(function () {
  targetPos = target.offset().top;
  winHeight = $(window).height();
  scrollToElem = targetPos - winHeight - 50;
});
$('#switcher-es').click(function () {
  targetPos = target.offset().top;
  winHeight = $(window).height();
  scrollToElem = targetPos - winHeight - 50;
});
//ÐŸÐµÑ€ÐµÐ½Ð¾Ñ Ðº Ð½ÐµÐ¾Ñ‚Ð²ÐµÑ‡ÐµÐ½Ð½Ñ‹Ð¼ End

$('.next-min').click(function () {
  var scrollName = $(this).attr('data-scroll');
  var scrollElem = $(scrollName);
  var scrollTop = scrollElem.offset().top-50;

  $('html, body').animate({
    scrollTop: scrollTop
  }, 500);
});

$('a').click(function () {
  if (this.hasAttribute('data-scroll')) {
    var scrollName = $(this).attr('data-scroll'),
      scrollElem = $(scrollName),
      scrollTop = scrollElem.offset().top-50;

    $('html, body').animate({
      scrollTop: scrollTop
    }, 500);
  };
});

//cookie
function getCookie(quizzPass) {
  var matches = document.cookie.match(new RegExp("(?:^|; )" + quizzPass.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
  return matches ? decodeURIComponent(matches[1]) : "18+";
}

if (getCookie('quizzPass') === 'quizzpass') {
  $('section:not(:first)').css('display', 'none');
  $('#slide-0').css('display', 'block');
  $('#slide-0 .stock-container').css('display', 'block');
  $('#slide-0 .stock-title').html('<b><span class="en">YOU HAVE ALREADY PASSED THE TEST</span><span class="es">YA HAS PASADO LA PRUEBA</span></b>');
  $('#slide-0 .stock-text:eq(1)').html('<span class="en">At the moment the priority in the study was given to the age group of <b><span class="howold-div">'+getCookie('howoldCookie')+'</span></b></span><span class="es">La prioridad en el estudio en este momento se dio al grupo de edad de <b><span class="howold-div">'+getCookie('howoldCookie')+'</span></b></span>');
  $('#doktoro').css('width', '175px');
  $('#description-quest').css('display', 'none');
  $('.question').css('display', 'none');
  $('.question-num').css('display', 'none');
  $('.choices').css('display', 'none');
  $('.stock').css('display', 'block');
  $('a').addClass('nonquizz');
  $('a').attr("href", "//" + offerDomain + "/click.php?lp=1");
  $('section').removeClass('item-correct');
  $('#ssbutton-fix').attr('id', 'ssbutton');
  $('#ssbutton').html('<span class="en">Get your 50% discount</span><span class="es">Obtenga su 50% de descuento</span>');
}
// end cookie
(function () {
	var body = document.getElementsByTagName('body')[0];

	var switcherEN = document.getElementById('switcher-en');
	var switcherES = document.getElementById('switcher-es');

	// Ð—Ð°Ñ€Ð°Ð½ÐµÐµ ÑÑ‡Ð¸Ñ‚Ð°ÐµÐ¼ ÑÑ€Ð¾Ðº Ñ…Ñ€Ð°Ð½ÐµÐ½Ð¸Ñ ÐºÑƒÐº
	var expires = new Date();
	expires.setDate(expires.getDate() + 366);
	expires.toUTCString();

	// Ð ÑƒÑÑÐºÐ¸Ð¹ Ð¸ Ð°Ð½Ð³Ð»Ð¸Ð¹ÑÐºÐ¸Ð¹ Ð·Ð°Ð³Ð¾Ð»Ð¾Ð²ÐºÐ¸
	// var titleEN = document.getElementById('title-en');
	// var titleES = document.getElementById('title-es');
	// var title = {
	// 	en: titleEN.getAttribute('content'),
	// 	es: titleES.getAttribute('content')
	// }

	var switchLang = function () {
		var lang = this.id
		lang = lang.replace('switcher-', '');

		// ÐœÐµÐ½ÑÐµÐ¼ ÐºÐ»Ð°ÑÑ Ð´Ð»Ñ BODY
		body.className = lang;
		
		// Ð—Ð°Ð¿Ð¸ÑÑ‹Ð²Ð°ÐµÐ¼ ÐºÑƒÐºÐ¸
		document.cookie = [
			'lang=' + lang
			, '; expires=' + expires // Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÐµÐ¼ expires, max-age Ð½Ðµ Ð¿Ð¾Ð´Ð´ÐµÑ€Ð¶Ð¸Ð²Ð°ÐµÑ‚ÑÑ Ð² Ð˜Ð•
			// , '; path=/'
			, '; domain=' + document.location.host
		].join('');
		// document.title = title[lang];
	}

	switcherEN.onclick = switchLang;
	switcherES.onclick = switchLang;
})();

//Ð”Ð¾ÑÑ‚Ð°ÐµÐ¼ ÐºÑƒÐºÐ¸
function getCookie(lang) {
	var matches = document.cookie.match(new RegExp("(?:^|; )" + lang.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	return matches ? decodeURIComponent(matches[1]) : "en";
}
document.getElementsByTagName('body')[0].className = getCookie('lang');

//ÐœÐµÐ½ÑŽ
var switcherButton = document.getElementById('site-settings-button');
var switcherMenu = document.getElementById('site-settings-menu');
var crossButton = document.getElementById('site-settings-cross');

var switchMenu = function () {
	if (switcherMenu.className == "site-settings-menu-none") {
		switcherMenu.className = "site-settings-menu";
		crossButton.innerHTML = ' &#10005;';
	} else {
		switcherMenu.className = "site-settings-menu-none";
		crossButton.innerHTML = ' &#9660;';
	}
}

switcherButton.onclick = switchMenu;

let click;
document.addEventListener("click", function (e) {
	// click = e.target;
	const target = e.target;
	const its_menu = target == switcherMenu || switcherMenu.contains(target);
	const its_btnMenu = target == switcherButton || switcherButton.contains(target);;
	const menu_is_active = switcherMenu.classList.contains('site-settings-menu');
	if (!its_menu && !its_btnMenu && menu_is_active) {
		switchMenu();
	}
});